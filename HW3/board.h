#ifndef BOARD_H
#define BOARD_H

//class pubBoard
//{
//public:
typedef unsigned char byte;
extern void print_board(void); //functions for print_board //changed from static to extern
inline int SQ(int f, int r);
inline char PIECE2CHAR(byte p);
inline int WTM();
extern void print_square(int square); //changed from static to extern
inline int F(int square);
inline int R(int square);
inline char FILE2CHAR(char f);
inline char RANK2CHAR(char r);
inline bool xisspace(int c); //functions for setup_board
extern void setup_board(const char *fen); //changed from static to extern
inline char CHAR2FILE(char c);
extern void reset(void); //changed from static to extern
struct move { //functions for reset()
    short move;
    unsigned short prescore;};
extern unsigned long compute_hash(void); //declarations for reset() //changed from static to extern
static struct move move_stack[1024], *move_sp;
static signed char undo_stack[6*1024], *undo_sp;
static unsigned long hash_stack[1024];
static unsigned long zobrist[12][64];
enum { RANK_1, RANK_2, RANK_3, RANK_4, RANK_5, RANK_6, RANK_7, RANK_8 }; //declarations ported from mscp for print_board
enum { FILE_A, FILE_B, FILE_C, FILE_D, FILE_E, FILE_F, FILE_G, FILE_H };
enum {                  /* 64 squares */
    A1, A2, A3, A4, A5, A6, A7, A8,
    B1, B2, B3, B4, B5, B6, B7, B8,
    C1, C2, C3, C4, C5, C6, C7, C8,
    D1, D2, D3, D4, D5, D6, D7, D8,
    E1, E2, E3, E4, E5, E6, E7, E8,
    F1, F2, F3, F4, F5, F6, F7, F8,
    G1, G2, G3, G4, G5, G6, G7, G8,
    H1, H2, H3, H4, H5, H6, H7, H8,
    CASTLE,         /* Castling rights */
    EP,             /* En-passant square */
    LAST};         /* Ply number of last capture or pawn push */
static int ply;
const int CASTLE_WHITE_KING = 1;
const int CASTLE_WHITE_QUEEN = 2;
const int CASTLE_BLACK_KING = 4;
const int CASTLE_BLACK_QUEEN = 8;
static byte board[64+3];
enum { //declarations ported from mscp for setup_board
	EMPTY,
	WHITE_KING, WHITE_QUEEN, WHITE_ROOK,
	WHITE_BISHOP, WHITE_KNIGHT, WHITE_PAWN,
	BLACK_KING, BLACK_QUEEN, BLACK_ROOK,
	BLACK_BISHOP, BLACK_KNIGHT, BLACK_PAWN};
//};






#endif